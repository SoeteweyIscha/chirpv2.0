﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteBehaviourScript : MonoBehaviour {

	// Use this for initialization


	public void playNote()
	{
		this.GetComponent<AudioSource>().Play();

		if (this.GetComponentsInChildren<Animator>().Length!=0)
		{
			this.GetComponentsInChildren<Animator>()[0].SetTrigger("Sing");
			this.GetComponentsInChildren<Animator>()[1].SetTrigger("Sing");
		}
		
	}

	// Update is called once per frame

}
