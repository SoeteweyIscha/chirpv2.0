﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetGameScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
	}

    public void ResetGame()
    {
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
}
