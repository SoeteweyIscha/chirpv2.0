﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionScript : MonoBehaviour
{
    public GameObject ContinueButton;
    [SerializeField]
    private SocketScript[] _sockets;

    //Used to determine wether the bird gets placed or not
    [SerializeField]
    private float _placeDist;

    private GameObject _selectedBird;
    private string _noteValue;
    private bool _holdingBird;
    private string _currentTag;

    private void Start()
    {
        _sockets = FindObjectsOfType<SocketScript>();
    }

    private void Update()
    {

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit = new RaycastHit();
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!_holdingBird)
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.tag == "Selector")
                    {
                        _selectedBird = hit.collider.gameObject.GetComponent<SelectorScript>().Object;
                        _currentTag = hit.collider.gameObject.GetComponent<SelectorScript>().NoteName;
                        _selectedBird.GetComponent<Collider>().enabled = false;
                        _holdingBird = true;
                        _selectedBird = Instantiate(_selectedBird, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
                    }
                }
            }

            else
            {
                Vector3 objectPos = new Vector3
                    (
                    Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 
                    Camera.main.ScreenToWorldPoint(Input.mousePosition).y, 
                    0);

                _selectedBird.transform.position = objectPos;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (_holdingBird == true)
            {
                float shortestDist = Mathf.Infinity;
                foreach (var child in _sockets)
                {
                    float currentDist = Vector3.Distance(child.gameObject.transform.position, _selectedBird.transform.position);

                    if (currentDist < _placeDist && currentDist < shortestDist)
                    {
                        

                        if (child.gameObject.tag == _currentTag)
                        {
                            shortestDist = currentDist;
                            _selectedBird.transform.position = child.gameObject.transform.position;
                            _selectedBird.GetComponent<Collider>().enabled = true;
                            child.gameObject.GetComponent<SocketScript>().SetBird(_selectedBird);
                            child.gameObject.GetComponent<SocketScript>().ActiveBird = true;
                            
                            CheckAllSockets();
                        }
                   
                    }
                }
                if (shortestDist > _placeDist)
                {
                    Destroy(_selectedBird);
                }
                _selectedBird = null;
                _holdingBird = false;
            }
        }


    }

 

    public void SetSelectedBird(string value)
    {
        // value is a note ranging from DO to dohigh
        _selectedBird = Resources.Load<GameObject>(value);
        _noteValue = value;
        Debug.Log(_selectedBird);
    }

    private void CheckAllSockets()
    {
        bool AllTrue = true;
        foreach (var s in _sockets)
        {
            if (s.ActiveBird == false)
            {
                AllTrue = false;
            }
        }

        if (AllTrue)
        {
            Debug.Log("Donezo");
            StartCoroutine("WaitForDelay");
        }
    }

    public IEnumerator WaitForDelay()
    {
        yield return new WaitForSeconds(5f);

        ContinueButton.SetActive(true);
        Debug.Log("Donezo");
    }
}
