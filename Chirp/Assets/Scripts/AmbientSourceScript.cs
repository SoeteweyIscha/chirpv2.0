﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSourceScript : MonoBehaviour {

	void Awake () {

        if (GameObject.FindGameObjectsWithTag("Ambient").Length > 1)
        {
            GameObject.Destroy(this);
        }
        else
        {
            Object.DontDestroyOnLoad(this);
        }
	}
}
