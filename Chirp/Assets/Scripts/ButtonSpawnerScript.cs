﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonSpawnerScript : MonoBehaviour {

    [SerializeField]
    private int StandardSquareSize = 75;

    private float _squareSize;
    private int _collumns;

    private RectTransform _panel;

    private int _sceneCount;
    private int _levelScenes;



    private GameObject _levelLoader;

	// Use this for initialization
	void Start () {
        _panel = GetComponent<RectTransform>();
        _sceneCount = SceneManager.sceneCountInBuildSettings;
        _levelLoader = (GameObject)Instantiate(Resources.Load("LevelLoader"));

        _collumns = (Mathf.FloorToInt(_panel.rect.width / StandardSquareSize));
        _squareSize = _panel.rect.width / _collumns;

        _collumns = Mathf.RoundToInt(_panel.rect.width / _squareSize);
        
        GetLevelCount();
        SpawnButtons();

        Debug.Log(_levelScenes);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void GetLevelCount()
    {
        for (int i = 0; i < _sceneCount; i++)
        {
            if (System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i)).Length <= 3)
            {
                _levelScenes += 1;
            }
        }
    }

    private void SpawnButtons()
    {
        for (int i = 1; i < _levelScenes + 1; i++)
        {
            GameObject levelLoader = Instantiate(_levelLoader, _panel);

            //Change the position on the screen
            RectTransform buttonTransform = levelLoader.GetComponent<RectTransform>();
            float x = 25 + ((i - 1) % _collumns) * _squareSize - _panel.rect.width / 2;
            float y = -25 +(Mathf.Floor((i - 1) / _collumns)) * _squareSize * -1 + _panel.rect.height / 2;
            buttonTransform.localPosition = new Vector3(x, y, 0);

            levelLoader.GetComponent<LoadLevelButtonScript>().Level = i;
            levelLoader.GetComponentInChildren<Text>().text = i.ToString();
            
            Debug.Log("Spawns Button");
        }
    }
}
