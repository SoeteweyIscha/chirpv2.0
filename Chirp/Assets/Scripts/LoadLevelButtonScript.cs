﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadLevelButtonScript : MonoBehaviour {

    public int Level;


    public void LoadLevel()
    {
        SceneManager.LoadScene(Level);
    }
}
