﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBehaviour : MonoBehaviour
{
    private int nextBuildIndex;
	// Use this for initialization
	void Start () {

	    nextBuildIndex = SceneManager.GetActiveScene().buildIndex + 1;
	    
    }

    public void LoadNext()
    {
        SceneManager.LoadScene(nextBuildIndex);
    }

    public void LoadLevelSelecter()
    {
        SceneManager.LoadScene("LevelSelect");
    }
}
