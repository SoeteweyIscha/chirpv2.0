﻿using UnityEngine;

public class SocketScript : MonoBehaviour {

    public GameObject _bird;


    public bool ActiveBird = false;

    public void SetBird(GameObject input)
    {
        _bird = input;
        ActiveBird = true;

    }

    public GameObject getBird()
    {
        return _bird;
    }
}
